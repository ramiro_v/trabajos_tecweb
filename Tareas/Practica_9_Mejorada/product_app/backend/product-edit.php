<?php
    include_once __DIR__.'/database.php';
   // SE OBTIENE LA INFORMACIÓN DEL PRODUCTO ENVIADA POR EL CLIENTE 
   $producto = file_get_contents('php://input');
   $jsonOBJ = json_decode($producto);

   $id = $jsonOBJ->id;
    $data = array(
        'status'  => 'error',
        'message' => 'Ya existe un producto con ese nombre'
    );
    if(!empty($id)) {

        $nombre = $jsonOBJ->nombre;
        $marca  = $jsonOBJ->marca;
        $modelo = $jsonOBJ->modelo;
        $precio = $jsonOBJ->precio;
        $detalles = $jsonOBJ->detalles;
        $unidades = $jsonOBJ->unidades;
        $imagen = $jsonOBJ->imagen;

    
        $sql = "UPDATE productos SET nombre='$nombre', marca='$marca', modelo='$modelo', precio='$precio', detalles='$detalles', unidades='$unidades', imagen='$imagen', eliminado='0' WHERE id='$id'";	    
        //$result = mysqli_query($conexion, $sql);
        if(!(mysqli_query($conexion, $sql))){
            die('Query Failed.');
            $data['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($conexion);
        }
        else{
            $data['status'] =  "success";
            $data['message'] =  "Producto ".$nombre." actualizado";
        }
        $conexion->close();

    }

    // SE HACE LA CONVERSIÓN DE ARRAY A JSON
    echo json_encode($data, JSON_PRETTY_PRINT);
?>