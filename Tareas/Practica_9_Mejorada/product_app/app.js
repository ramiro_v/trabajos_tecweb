var Nombre_File; //NOMBRE DE LA IMAGEN 

const expresionCampo = {
	nombre: /^[a-z0-9_,A-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos. De 1 a 100 caracteres
    modelo: /^[a-z0-9_,A-ZÀ-ÿ\s]{1,24}$/,
    unidades: -1, //EXPRESION EN LA QUE EL CAMPO ES CORRECTO
    precio: 99.99,
    detalles: /^[a-z0-9_,A-ZÀ-ÿ\s]{0,255}$/
}
var mensajeERROR = {
    nombre: 'El nombre debe ser menor a 100 caracteres',
    marca: 'Debes elegir alguna opción',
    modelo: 'El modelo debe ser menor a 25 caracteres',
    unidades:'Las unidades deben ser mayores o iguales a 0',
    precio: 'El precio debe ser mayor a $99.99',
    detalles: 'Los detalles deben ser menores a 255 caracteres'
}

var camposV = {
	nombre: false,
	marca: false,
	modelo: false,
	unidades: false,
    precio: false,
    detalles: true
}

$(document).ready(function(){
    
    let editar = false;
    
    listarProductos();
    $('#product-result').hide();
    $('#form-input-error').hide();

    function listarProductos(){
        $.ajax({
            url: 'backend/product-list.php',
            type: 'GET',
            success: function(respuesta) {
                const productos = JSON.parse(respuesta);
                //console.log(respuesta);
                let template = '';
                for (let i=0; i < productos.length; i++){
                    let descripcion = '';
                    //SE ENLISTAN LOS DETALLES DEL PRODUCTO
                    descripcion += '<li>Precio: '+productos[i].precio+'</li>';
                    descripcion += '<li>Unidades: '+productos[i].unidades+'</li>';
                    descripcion += '<li>Modelo: '+productos[i].modelo+'</li>';
                    descripcion += '<li>Marca: '+productos[i].marca+'</li>';
                    descripcion += '<li>Detalles: '+productos[i].detalles+'</li>';
                    descripcion += '<li>Imagen: '+productos[i].imagen+'</li>';
                    //SE CREAN LAS FILAS
                    template += `
                        <tr productID="${productos[i].id}">
                            <td>${productos[i].id}</td>
                            <td><a href="#" class="product-edit">${productos[i].nombre}</a></td>
                            <td><ul>${descripcion}</ul></td>
                            <td><button class="producto-eliminado btn btn-danger">Eliminar</button></td>
                        </tr>
                    `;
                }
                $('#products').html(template);
            }
        });
    }

    $('#search').keyup(function(){
        if($('#search').val()){
            let search = $('#search').val();
            $.ajax({
                url: 'backend/product-search.php',
                data: {search},
                type: 'POST',
                success: function(respuesta){
                    console.log("RESPUESTA "+respuesta);
                    if(!respuesta.error){
                        let productos = JSON.parse(respuesta);
                        let template = '';
                        let nombresw ='';
                        //console.log(respuesta);
                        console.log("tamaño: "+productos.length);
                        for (let i=0; i < productos.length; i++){
                            let descripcion = '';
                            //SE ENLISTAN LOS DETALLES DEL PRODUCTO
                            descripcion += '<li>Precio: '+productos[i].precio+'</li>';
                            descripcion += '<li>Unidades: '+productos[i].unidades+'</li>';
                            descripcion += '<li>Modelo: '+productos[i].modelo+'</li>';
                            descripcion += '<li>Marca: '+productos[i].marca+'</li>';
                            descripcion += '<li>Detalles: '+productos[i].detalles+'</li>';
                            descripcion += '<li>Imagen: '+productos[i].imagen+'</li>';
                            //SE CREAN LAS FILAS
                            template += `
                                <tr productID="${productos[i].id}">
                                    <td>${productos[i].id}</td>
                                    <td><a href="#" class="product-edit">${productos[i].nombre}</a></td>
                                    <td><ul>${descripcion}</ul></td>
                                    <td><button class="producto-eliminado btn btn-danger">Eliminar</button></td>
                                </tr>
                            `
                            nombresw += `
                                <li>${productos[i].nombre}</li>
                           `
                        }
                        console.log(nombresw);

                        $('#products').html(template);
                        if(productos.length == 0){
                            $('#product-result').show();
                            $('#container').html("NO SE ENCONTRARON COINCIDENCIAS");
                        }
                        else{
                            $('#product-result').show();
                            $('#container').html("Coincidencias:<br>"+nombresw);
                        }
                    }
                }
            })
        }
        else{
            listarProductos();
            $('#product-result').hide();
        }
    });

    $(document).on('click','.producto-eliminado',(e) =>{
        if(confirm('¿Deseas eliminar el producto?')){
            const element = $(this)[0].activeElement.parentElement.parentElement;
            const id = $(element).attr('productID');
            $.post('backend/product-delete.php', {id}, (res) => {
                mensaje = JSON.parse(res);              
                $('#product-result').show();
                if(mensaje.status == 'success'){
                    $('#container').html("Estatus: "+mensaje.status+"<br> Mensaje: "+mensaje.message+"con ID: "+id);
                }
                else{
                    $('#container').html("Estatus: "+mensaje.status+"<br> Mensaje: "+mensaje.message);
                }
                listarProductos();
            });
        }
    });

    $('#product-form').submit(function (e){
        e.preventDefault();
        if(camposV.nombre && camposV.precio && camposV.unidades && camposV.modelo && camposV.marca){
            console.log(camposV.nombre + ' LISTO PARA ENVIAR');
            img = Nombre_File === ''? 'img/default.png' : 'img/'+Nombre_File;
            var Json = {
                "id": $('#productId').val(),
                "nombre": $('#nombre').val(),
                "precio": $('#precio').val(),
                "unidades": $('#unidades').val(),
                "modelo": $('#modelo').val(),
                "marca": $('#marca').val(),
                "detalles": $('#detalles').val(),
                "imagen": img
            };
            const postData = JSON.stringify(Json,null,2);
            console.log("DATA: "+postData);
            let url = editar === false ? 'backend/product-add.php' : 'backend/product-edit.php';
            console.log("EDITAR: "+editar);
            $.post(url, postData, (response) => {
                console.log("RESPUESTA: "+response);
                mensaje = JSON.parse(response);              
                $('#product-result').show();
                console.log(response);
                $('#container').html("Estatus: "+mensaje.status+"<br> Mensaje: "+mensaje.message);
                listarProductos();
                //init();
            });
        }
        else{
            console.log('No se envia');
            $('#product-result').show();
            $('#container').html("Campos Invalidos: No se puede agregar el producto");
            for (let i in camposV){
                if(!camposV[i]){
                    estadoCampo('incorrecto',i,'ERROR: El campo debe llenarse correctamente');
                }
            }
        }
    });

    

    //ESCRIBIR EN EL FORMULARIO NOMBRE Y SABER SI YA EXISTE
    $('#nombre').keyup(function(){
        if($('#nombre').val()){
            let name = $('#nombre').val();
            $.ajax({
                url: 'backend/product-name.php',
                data: {name},
                type: 'POST',
                success: function(respuesta){
                    if(!respuesta.error){
                        let status = respuesta === "YES" ? 'incorrecto' : 'correcto';
                        estadoCampo(status,'nombre','ERROR: El producto ya existe en la BD');
                    }
                }
            });
        }
        else{
            estadoCampo('incorrecto','nombre','ERROR: El campo nombre no puede quedar vacio');
        }
    });
    //VALIDAR QUE LOS INPUTS NO QUEDEN VACIOS
    datosVacios('modelo');
    datosVacios('unidades');
    datosVacios('precio');
    datosVacios('nombre');

    //VALIDAR QUE SE CUMPLAN LAS RESTRICCIONES DE INPUTS
    teclearCampo('modelo');
    teclearCampo('detalles');
    cambioCampo('unidades');
    cambioCampo('precio');
    /// #####  IMAGEN
    $('#imagen').change(function(){
        if($('#imagen').val()!=''){
            let nombre_imagen = $('#imagen').val();
            nombre_imagen = nombre_imagen.replace(/^.*[\\\/]/, '');
            $('#estado-imagen').html(nombre_imagen);
            Nombre_File = nombre_imagen;
        }
    });

    $('#imagen').click(function(){
        if($('#imagen').val()!=''){
            $('#estado-imagen').html('Seleccionar Imagen');
            $('#imagen').val()='';
            Nombre_File='';
        }
    });
    /// ##### MARCA
    $('#marca').change(function(){
        if($('#marca').val()!=''){
            estadoCampo('correcto','marca','')
        }
        else{
            estadoCampo('incorrecto','marca',mensajeERROR['marca']);
        }
    });

//HACER CLICK SOBRE EL NOMBRE DEL PRODUCTO PARA EDITAR
    $(document).on('click','.product-edit',(e)=>{
        const element = $(this)[0].activeElement.parentElement.parentElement;
        const id = $(element).attr('productID');
        e.preventDefault();
        console.log("EDITAR id:"+id);
        $.post('backend/product-id.php',{id},(respuesta)=>{
            //console.log("RESPUESTA "+respuesta);
            const product = JSON.parse(respuesta);
            $('#nombre').val(product[0].nombre);
            $('#precio').val(product[0].precio);
            $('#productId').val(product[0].id);
            $('#unidades').val(product[0].unidades);
            $('#modelo').val(product[0].modelo);
            $('#marca').val(product[0].marca);
            $('#detalles').val(product[0].detalles);
            for (let i in camposV){
                estadoCampo('correcto',i,'');
            }
            let nombre_imagen = product[0].imagen;
            nombre_imagen = nombre_imagen.replace(/^.*[\\\/]/, '');
            $('#estado-imagen').html(nombre_imagen);
            Nombre_File = nombre_imagen;
            editar = true;
        });
    });

});

function estadoCampo(estado, campo, mensaje){
    if(estado=="incorrecto")
    {
        $(`#validar-estado-${campo}`).addClass("form-group-input-incorrecto");
        $(`#validar-estado-${campo}`).removeClass("form-group-input-correcto");
        $(`#validar-estado-${campo} ~ #form-input-error`).show();
        $(`#validar-estado-${campo} ~ #form-input-error`).html(`${mensaje}`);
        camposV[campo] = false;
    }
    else{
        $(`#validar-estado-${campo}`).removeClass("form-group-input-incorrecto");
        $(`#validar-estado-${campo}`).addClass("form-group-input-correcto");
        $(`#validar-estado-${campo} ~ #form-input-error`).hide();
        camposV[campo] = true;
    }
}

function datosVacios(campo){
    //console.log("------");
    $(`#${campo}`).blur(function(){
        if($(`#${campo}`).val()==''){
            estadoCampo('incorrecto',campo,`ERROR: EL campo ${campo} no puede quedar vacio`);
        }
    });
}

function teclearCampo(campo){
    //console.log("teclear");
    $(`#${campo}`).keyup(function(){
        //console.log("Teclear2");
        if(expresionCampo[campo].test($(`#${campo}`).val())){
            estadoCampo('correcto',campo,'');
        }
        else{
            estadoCampo('incorrecto',campo,mensajeERROR[campo]);
        }
    });
}

function cambioCampo(campo){
    
    if(campo == 'marca'){
        $('#marca').change(function(){
            if($('#marca').val()!=''){
                estadoCampo('correcto','marca','')
            }
            else{
                estadoCampo('incorrecto','marca',mensajeERROR['marca']);
            }
        });
    }
    else{
        $(`#${campo}`).change(function(){
            if($(`#${campo}`).val()>expresionCampo[campo]){
                estadoCampo('correcto',campo,'');
            }
            else{
                estadoCampo('incorrecto',campo,mensajeERROR[campo]);
            }
        });
    }
}
