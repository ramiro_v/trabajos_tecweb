<?php
    use ProductosBD\Productos;
    include_once __DIR__.'/API/Productos.php';
    $id = $_POST['id']; 
    if( isset($id) ) {
        $productos = new Productos('marketzone');
        $productos->byid($id);
        echo $productos->getResponse();
    } 
?>