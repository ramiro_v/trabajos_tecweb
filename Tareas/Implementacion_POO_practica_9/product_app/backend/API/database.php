<?php
    namespace Productos_BD;
    abstract class DataBase{
        
        protected $conexion;

        public function __construct($name_bd){
            $this->conexion = @mysqli_connect('localhost','root','VIDAL2207l',$name_bd);
        }

        public function getConexion(){
            if(!$this->conexion) {
                die('¡Base de datos NO conectada!');
            }
            return $this->conexion;
        }
        public function __destruct(){
            $this->conexion->close();
        }
    }

?>