<?php
    namespace ProductosBD;
    use Productos_BD\DataBase;
    include_once __DIR__.'/database.php';

    class Productos extends DataBase{

        private $response = array();

        public function lista(){
            if ( $result = $this->conexion->query("SELECT * FROM productos WHERE eliminado = 0") ) {
                $rows = $result->fetch_all(MYSQLI_ASSOC);
                if(!is_null($rows)) {
                    foreach($rows as $num => $row) {
                        foreach($row as $key => $value) {
                            $this->response[$num][$key] = utf8_encode($value);
                        }
                    }
                } else{
                    $this->response = array(
                        'status:'  => 'ERROR',
                        'message:' => 'No hay productos'
                    );
                }
                $result->free();
            } else {
                die('Query Error: '.mysqli_error($this->conexion));
            }
        }

        public function add($producto){   
            $this->response = array(
                'status'  => 'error',
                'message' => 'Ya existe un producto con ese nombre'
            );
            if(!empty($producto)) {
                $jsonOBJ = json_decode($producto);
                $this->conexion->set_charset("utf8");
                $sql = "INSERT INTO productos VALUES (null, '{$jsonOBJ->nombre}', '{$jsonOBJ->marca}', '{$jsonOBJ->modelo}', {$jsonOBJ->precio}, '{$jsonOBJ->detalles}', {$jsonOBJ->unidades}, '{$jsonOBJ->imagen}', 0)";
                if($this->conexion->query($sql)){
                    $this->response['status'] =  "success";
                    $this->response['message'] =  "Producto agregado";
                } else {
                    $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
                }
            }
        }

        public function delete($id){
            $this->response = array(
                'status'  => 'error',
                'message' => 'La consulta falló'
            );
            if($id!=''){
                $sql = "UPDATE productos SET eliminado=1 WHERE id = {$id}";
                if ( $this->conexion->query($sql) ) {
                    $this->response['status'] =  "success";
                    $this->response['message'] =  "Producto eliminado";
                } else {
                    $this->response['status'] =  "error";
                    $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
                }
            } 
        }

        public function edit($producto){
            $this->response = array(
                'status'  => 'error',
                'message' => 'Error en el producto'
            );
            $jsonOBJ = json_decode($producto);
            $id = $jsonOBJ->id;
             if(!empty($id)) {
                $nombre = $jsonOBJ->nombre;
                $marca  = $jsonOBJ->marca;
                $modelo = $jsonOBJ->modelo;
                $precio = $jsonOBJ->precio;
                $detalles = $jsonOBJ->detalles;
                $unidades = $jsonOBJ->unidades;
                $imagen = $jsonOBJ->imagen;
                $sql = "UPDATE productos SET nombre='$nombre', marca='$marca', modelo='$modelo', precio='$precio', detalles='$detalles', unidades='$unidades', imagen='$imagen', eliminado='0' WHERE id='$id'";	
                if(!($this->conexion->query($sql))){
                    die('Query Failed.');
                    $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
                }
                else{
                    $this->response['status'] =  "success";
                    $this->response['message'] =  "Producto ".$nombre." actualizado";
                }   
             }
        }

        public function search($search){
            $sql = "SELECT * FROM productos WHERE (id = '{$search}' OR nombre LIKE '{$search}%' OR marca LIKE '{$search}%' OR detalles LIKE '{$search}%') AND eliminado = 0";
            if ( $result = $this->conexion->query($sql) ) {
                $rows = $result->fetch_all(MYSQLI_ASSOC);
                if(!is_null($rows)) {
                    foreach($rows as $num => $row) {
                        foreach($row as $key => $value) {
                            $this->response[$num][$key] = utf8_encode($value);
                        }
                    }
                }
                $result->free();
            } else {
                die('Query Error: '.mysqli_error($this->conexion));
            }
        }

        public function byname($name){
            $this->response['status'] =  "error";
            $sql = "SELECT * FROM productos WHERE nombre = '{$name}'";
            if ( $result = $this->conexion->query($sql) ) {
                $row = $result->fetch_array(MYSQLI_ASSOC);
                if(!is_null($row)) {
                    $this->response['status'] =  "success";
                }
                $result->free();
            } else {
                die('Query Error: '.mysqli_error($this->conexion));
            }
        } 
        
        public function byid($id){
            if ( $result = $this->conexion->query("SELECT * FROM productos WHERE id ={$id}") ) {
                $rows = $result->fetch_all(MYSQLI_ASSOC);
                if(!is_null($rows)) {
                    foreach($rows as $num => $row) {
                        foreach($row as $key => $value) {
                            $this->response[$num][$key] = utf8_encode($value);
                        }
                    }
                }
                else{
                    $this->response = array(
                        'Estatus:'  => 'ERROR',
                        'Mensaje:' => 'No hay productos'
                    );
                }
                $result->free();
            } else {
                die('Query Error: '.mysqli_error($this->conexion));
            }
        }

        public function getResponse(){
            return json_encode($this->response,JSON_PRETTY_PRINT);
        }
    }

?>