<?php
    use ProductosBD\Productos;
    include_once __DIR__.'/API/Productos.php';
    $search = $_POST['search']; 
    if( isset($search) ) {
        $productos = new Productos('marketzone');
        $productos->search($search);
        echo $productos->getResponse();
    } 
?>