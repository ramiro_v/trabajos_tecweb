<?php
    use ProductosBD\Productos;
    include_once __DIR__.'/API/Productos.php';
    $name = $_POST['name']; 
    if( isset($name) ) {
        $productos = new Productos('marketzone');
        $productos->byname($name);
        echo $productos->getResponse();
    } 
?>