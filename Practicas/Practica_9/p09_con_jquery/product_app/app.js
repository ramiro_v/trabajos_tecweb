var baseJSON = {
    "precio": 0.0,
    "unidades": 1,
    "modelo": "XX-000",
    "marca": "NA",
    "detalles": "NA",
    "imagen": "img/default.png"
  };

function init() {
    var JsonString = JSON.stringify(baseJSON,null,2);
    document.getElementById("name").value="";
    document.getElementById("description").value = JsonString;
    console.log("INIT");
}
$(document).ready(function(){
    
    let editar = false;
    
    listarProductos();
    $('#product-result').hide();

    function listarProductos(){
        $.ajax({
            url: 'backend/product-list.php',
            type: 'GET',
            success: function(respuesta) {
                const productos = JSON.parse(respuesta);
                //console.log(respuesta);
                let template = '';
                for (let i=0; i < productos.length; i++){
                    let descripcion = '';
                    //SE ENLISTAN LOS DETALLES DEL PRODUCTO
                    descripcion += '<li>Precio: '+productos[i].precio+'</li>';
                    descripcion += '<li>Unidades: '+productos[i].unidades+'</li>';
                    descripcion += '<li>Modelo: '+productos[i].modelo+'</li>';
                    descripcion += '<li>Marca: '+productos[i].marca+'</li>';
                    descripcion += '<li>Detalles: '+productos[i].detalles+'</li>';
                    //SE CREAN LAS FILAS
                    template += `
                        <tr productID="${productos[i].id}">
                            <td>${productos[i].id}</td>
                            <td><a href="#" class="product-edit">${productos[i].nombre}</a></td>
                            <td><ul>${descripcion}</ul></td>
                            <td><button class="producto-eliminado btn btn-danger">Eliminar</button></td>
                        </tr>
                    `;
                }
                $('#products').html(template);
            }
        });
    }

    $('#search').keyup(function(){
        if($('#search').val()){
            let search = $('#search').val();
            $.ajax({
                url: 'backend/product-search.php',
                data: {search},
                type: 'POST',
                success: function(respuesta){
                    console.log("RESPUESTA "+respuesta);
                    if(!respuesta.error){
                        let productos = JSON.parse(respuesta);
                        let template = '';
                        let nombresw ='';
                        //console.log(respuesta);
                        console.log("tamaño: "+productos.length);
                        for (let i=0; i < productos.length; i++){
                            let descripcion = '';
                            //SE ENLISTAN LOS DETALLES DEL PRODUCTO
                            descripcion += '<li>Precio: '+productos[i].precio+'</li>';
                            descripcion += '<li>Unidades: '+productos[i].unidades+'</li>';
                            descripcion += '<li>Modelo: '+productos[i].modelo+'</li>';
                            descripcion += '<li>Marca: '+productos[i].marca+'</li>';
                            descripcion += '<li>Detalles: '+productos[i].detalles+'</li>';
                            //SE CREAN LAS FILAS
                            template += `
                                <tr productID="${productos[i].id}">
                                    <td>${productos[i].id}</td>
                                    <td><a href="#" class="product-edit">${productos[i].nombre}</a></td>
                                    <td><ul>${descripcion}</ul></td>
                                    <td><button class="producto-eliminado btn btn-danger">Eliminar</button></td>
                                </tr>
                            `
                            nombresw += `
                                <li>${productos[i].nombre}</li>
                           `
                        }
                        console.log(nombresw);

                        $('#products').html(template);
                        if(productos.length == 0){
                            $('#product-result').show();
                            $('#container').html("NO SE ENCONTRARON COINCIDENCIAS");
                        }
                        else{
                            $('#product-result').show();
                            $('#container').html("Coincidencias:<br>"+nombresw);
                        }
                    }
                }
            })
        }
        else{
            listarProductos();
            $('#product-result').hide();
        }
    });

    $(document).on('click','.producto-eliminado',(e) =>{
        if(confirm('¿Deseas eliminar el producto?')){
            const element = $(this)[0].activeElement.parentElement.parentElement;
            const id = $(element).attr('productID');
            $.post('backend/product-delete.php', {id}, (res) => {
                mensaje = JSON.parse(res);              
                $('#product-result').show();
                if(mensaje.status == 'success'){
                    $('#container').html("Estatus: "+mensaje.status+"<br> Mensaje: "+mensaje.message+"con ID: "+id);
                }
                else{
                    $('#container').html("Estatus: "+mensaje.status+"<br> Mensaje: "+mensaje.message);
                }
                listarProductos();
            });
        }
    });

    $('#product-form').submit(function (e){
        e.preventDefault();
        let Json = JSON.parse($('#description').val());
        Json['nombre'] = $('#name').val();
        Json['id'] = $('#productId').val();
        const postData = JSON.stringify(Json,null,2);

        if(Json['nombre'] == 0 || Json['nombre'].length >100){
            alert('Nombre: Valor incorrecto');
            return false;
        }
        if(Json.precio < 100){
            alert('Precio incorrecto');
            return false;
        }
        console.log("postDATA: "+postData);
        let url = editar === false ? 'backend/product-add.php' : 'backend/product-edit.php';
        console.log("EDITAR: "+editar);
        $.post(url, postData, (response) => {
            console.log("RESPUESTA: "+response);
            mensaje = JSON.parse(response);              
            $('#product-result').show();
            console.log(response);
            $('#container').html("Estatus: "+mensaje.status+"<br> Mensaje: "+mensaje.message);
            listarProductos();
            init();
        });
    });
//HACER CLICK SOBRE EL NOMBRE DEL PRODUCTO
    $(document).on('click','.product-edit',(e)=>{
        const element = $(this)[0].activeElement.parentElement.parentElement;
        const id = $(element).attr('productID');
        e.preventDefault();
        console.log("EDITAR id:"+id);
        $.post('backend/product-id.php',{id},(respuesta)=>{
            console.log("RESPUESTA "+respuesta);
            const product = JSON.parse(respuesta);
            var baseJSON_2 = {
                "precio": product[0].precio,
                "unidades": product[0].unidades,
                "modelo": product[0].modelo,
                "marca": product[0].marca,
                "detalles": product[0].detalles,
                "imagen": product[0].imagen
              };
            $('#description').val(JSON.stringify(baseJSON_2,null,2));
            $('#name').val(product[0].nombre);
            $('#productId').val(product[0].id);
            //console.log("p: "+baseJSON);
            //console.log("NOMBRE PRODUCT: "+nombre);}
            editar = true;
        });
    });

});
