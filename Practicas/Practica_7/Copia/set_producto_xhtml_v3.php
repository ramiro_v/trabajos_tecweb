<?php
    $id = $_POST['id_producto'];
    $nombre = $_POST['nombre'];
    $marca  = $_POST['marca'];
    $modelo = $_POST['modelo'];
    $precio = $_POST['precio'];
    $detalles = $_POST['detalles'];
    $unidades = $_POST['unidades'];
    $imgContenido = $_POST['file'];
    $imgContenido = 'img/'.$imgContenido;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <link rel="stylesheet" type="text/css" href="css/estilos.css">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Practica 6</title>
  </head>
  <body>
      <header class="header">
        <nav class="nav">
          <a href="formulario_productos_v2.html" class="logo nav-link">MarketPlace</a>
          <ul class="nav-menu">
            <li class="nav-menu-item">
              <a href="formulario_productos_v2.html" class="nav-menu-link nav-link active">Registrar</a>
            </li>
            <li class="nav-menu-item">
              <a href="get_productos_vigentes_v2" class="nav-menu-link nav-link">Mostrar</a>
            </li>
          </ul>
        </nav>
      </header>
      <section class="section">
      <?php
            @$link = new mysqli('localhost', 'root', 'VIDAL2207l', 'marketzone');
            if ($link->connect_errno) 
            {
                die('Falló la conexión: '.$link->connect_error.'<br/>');
                /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */
            }
            $sql = "UPDATE productos SET nombre='$nombre', marca='$marca', modelo='$modelo',precio='$precio',detalles='$detalles',unidades='$unidades',imagen='$imgContenido',eliminado='0' WHERE id='$id'";
            
            
            if ( $link->query($sql) ) 
            {
//cerrad?> 
                <h2 class="titulo">Producto actualizado:</h2><br><br>
                <div class="formulario">
                    <p class="muestra">Producto editado con ID: <?php echo $id; ?></p>
                    <p class="muestra">Nombre: <?php echo $nombre; ?></p>
                    <P class="muestra">Marca: <?php echo $marca; ?></p>
                    <p class="muestra">Modelo: <?php echo $modelo; ?></p>
                    <p class="muestra">Precio: <?php echo $precio; ?></p>
                    <p class="muestra">Dettalles: <?php echo $detalles; ?></p>
                    <p class="muestra">Unidades: <?php echo $unidades; ?></p>
                    <p class="muestra">Imagen: <?php echo $imgContenido; ?></p>
                </div>
                <form action="get_productos_vigentes_v2.php">
                    <button type="submit" class="boton">Ver Cambios</button>
                </form>
        <?php //ABIERTO 
            }
            else{
              echo '<script type="text/javascript">';
              echo'alert("No se logro editar el producto con ID: '.$id.'")';
              echo 'alert("nombre='.$nombre.', marca='.$marca.', modelo='.$modelo.',precio='.$precio.',detalles='.$detalles.',unidades='.$unidades.',imagen='.$imgContenido.',eliminado=0 WHERE id='.$id.'")';
              echo'</script>';
              include ('get_productos_vigentes_v2.php');
            }
            $link->close();
          ?>
      </section>
    </body>
  <footer class="footer">
    <div>
      <p>&copy; Copyright<strong> Ramiro Vidal Lumbreras</strong></p>
    </div>
  </footer>
</html>