<!DOCTYPE html>
<html lang="en">
  <?php
    $id_producto = $_POST['id'];
    $data = array();
    /** SE CREA EL OBJETO DE CONEXION */
    @$link = new mysqli('localhost', 'root', 'VIDAL2207l', 'marketzone');	

    /** comprobar la conexión */
    if ($link->connect_errno) 
    {
        die('Falló la conexión: '.$link->connect_error.'<br/>');
            /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */
    }
    else{
      $result = $link->query("SELECT * FROM productos WHERE id = {$id_producto}");
      $row = $result->fetch_all(MYSQLI_ASSOC);
      foreach($row as $num => $registro) {            // Se recorren tuplas
        foreach($registro as $key => $value) {      // Se recorren campos
            $data[$num][$key] = utf8_encode($value);
        }
      }
      $data = $row;
      /** útil para liberar memoria asociada a un resultado con demasiada información */
      $result->free();
    }

    $link->close();
  ?>
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <link rel="stylesheet" type="text/css" href="css/estilos.css">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Practica 6</title>
  </head>
  <script type="text/javascript">
    function validar(Id_p)
    {
        var Form = document.FormularioProducto;
        if(Form.nombre.value==0){
            alert('Campo Nombre: Valor incorrecto');
            Form.nombre.value="";
            Form.nombre.focus();
            return false;
        }
        if(Form.marca.value==0){
            alert('Campo Marca: Valor incorrecto');
            Form.marca.value="";
            Form.marca.focus();
            return false;
        }
        if(Form.modelo.value==0){
            alert('Campo Modelo: Valor incorrecto');
            Form.modelo.value="";
            Form.modelo.focus();
            return false;
        }
        if(Form.detalles.value==0){
            alert('Campo Detalles: Valor incorrecto');
            Form.detalles.value="";
            Form.detalles.focus();
            return false;
        }
        if(Form.precio.value==0){
            alert('Campo Precio: Precio incorrecto');
            Form.precio.value="";
            return false;
        }
        if(Form.unidades.value<0){
            alert('Campo Unidades: Valor incorrecto');
            Form.unidades.value="";
            return false;
        }
        if(Form.file.value==0){ 
            alert('Imagen no colocada');
            Form.file.value="";
            return false;
        }
        Form.submit();
    }
  </script>
  <body>
      <header class="header">
        <nav class="nav">
          <a href="formulario_productos_v2.html" class="logo nav-link">MarketPlace</a>
          <ul class="nav-menu">
            <li class="nav-menu-item">
              <a href="formulario_productos_v2.html" class="nav-menu-link nav-link active">Registrar</a>
            </li>
            <li class="nav-menu-item">
              <a href="get_productos_vigentes_v2.php" class="nav-menu-link nav-link">Mostrar</a>
            </li>
          </ul>
        </nav>
      </header>
      <section class="section">
        <h2 class="titulo">Editar producto</h2>
        <div class="formulario">
          <form name="FormularioProducto" action="set_producto_xhtml_v3.php" method="post" onsubmit="return validar();">
            <div class="form-fila">
            <div class="form-group">
                <label>ID: <?php echo($id_producto)?></label>
                  <input type="hidden" name="id_producto" value="<?php echo($id_producto)?>">
              </div>
              <br>
              <div class="form-group">
                <label>Nombre:</label>
                  <input type="text" name="nombre" class="form-input" value="<?php echo($data[0]['nombre'])?>">
              </div>
              <div class="form-group">
                <label>Marca:</label>
                  <input type="text" name="marca" class="form-input" value="<?php echo($data[0]['marca'])?>">
              </div>
              <div class="form-group">
                <label>Modelo:</label>
                <input type="text" name="modelo" class="form-input" value="<?php echo($data[0]['modelo'])?>">
              </div> 
            </div>
            <div class="form-fila">
              <div class="form-group">
                <label>Detalles:</label>
                  <input type="text" name="detalles" class="form-input" value="<?php echo($data[0]['detalles'])?>">
              </div>
              <div class="form-group">
                <label>Precio:</label>
                  <input type="number" name="precio" class="form-input" value="<?php echo($data[0]['precio'])?>" step="0.01" min="0.01">
              </div>
              <div class="form-group">
                <label>Unidades: </label>
                  <input type="number" name="unidades" class="form-input" min="1" value="<?php echo($data[0]['unidades'])?>">
              </div>
              <div class="form-group">
                <!--<label>Imagen del producto: <?php #echo($data[0]['imagen'])?></label>-->
                <input class="visible" type="file" id="file" name="file" value="<?php #echo($data[0]['imagen'])?>">
              </div>
              <br>
            </div>
            <button type="submit" class="boton" onclick="validar(<?php echo($id_producto); ?>)">Editar producto</button>
          </form>
        </div>
      </section>
    </body>
</html>