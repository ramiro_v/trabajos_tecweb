function vaciar(control)
{
    control.placeholder = '';
}

function entrada_nombre(name){
    if(name.value.length > 100){
        alert('El campo nombre debe ser menor o igual a 100 caracteres');
    }
}

function entrada_marca(marca){
    if(marca.options[marca.selectedIndex].value == "n"){
        alert('Opciones no es una opción valida');
    }
}

function entrada_modelo(modelo){
    if(modelo.value.length > 25){
        alert('El campo modelo debe ser menor o igual a 25 caracteres');
    }
}

function entrada_detalles(det){
    if(det.value.length > 250){
        alert('Los detalles no deben superar los 250 caracteres');
    }
}

function entrada_precio(precio){
    if(precio.value < 100){
        alert('No se aceptan precios menores a $100');
    }
}

function entrada_unidades(unidad){
    if(unidad.value < 0){
        alert('No se aceptan valores negativos');
    }
}

function validar()
{
    var Form = document.FormularioProducto;
    if(Form.nombre.value==0 || Form.nombre.value.length>100){
        alert('Campo Nombre: Valor incorrecto');
        Form.nombre.value="";
        Form.nombre.focus();
        return false;
    }
    if(Form.marca.value=='n'){
        alert('Campo Marca: Opciones es incorrecto');
        Form.marca.value="n";
        Form.marca.focus();
        return false;
    }
    if(Form.modelo.value==0 || Form.modelo.value.length>25){
        alert('Campo Modelo: Valor incorrecto');
        Form.modelo.value="";
        Form.modelo.focus();
        return false;
    }
    if(Form.detalles.value.length>250){
        alert('Campo Detalles: Superaste el número de caracteres');
        Form.detalles.value="";
        Form.detalles.focus();
        return false;
    }
    if(Form.precio.value==0 || Form.precio.value<100){
        alert('Campo Precio: Precio incorrecto');
        Form.precio.value="";
        return false;
    }
    if(Form.unidades.value<0){
        alert('Campo Unidades: Valor incorrecto');
        Form.unidades.value="";
        return false;
    }
    if(Form.file.value==0 || Form.file.value==NULL || Form.file.value==''){ //NO FUNCIONA
        Form.file.value = "no_hay_imagen.png";
    }
    alert('Datos enviados con exito');
    Form.submit();
}
