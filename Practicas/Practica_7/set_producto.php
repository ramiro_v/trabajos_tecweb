<?php
    $nombre = $_POST['nombre'];
    $marca  = $_POST['marca'];
    $modelo = $_POST['modelo'];
    $precio = $_POST['precio'];
    $detalles = $_POST['detalles'];
    $unidades = $_POST['unidades'];
    $imagen = $_POST['file'];
    $imagen = 'imagenes/celulares/'.$imagen;
    if($imagen == "imagenes/celulares/"){
      $imagen = 'imagenes/celulares/no_hay_imagen.png';
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
  <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <title>Tienda Online</title>
      <link rel="stylesheet" type="text/css" href="css/estilo.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap/styles.css">
      <link rel="icon" type="image/x-icon" href="imagenes/iconos/tienda.png"/>
      <script type="text/javascript" src="js/alertas.js"></script>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <div class="container px-4 px-lg-5">
          <a class="navbar-brand" href="#"><img class="logo" src="imagenes/logo.png" >Tienda Online</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                  <li class="nav-item"><a class="nav-link active" aria-current="page" href="construccion.html">Home</a></li>
                  <li class="nav-item"><a class="nav-link active" aria-current="page" href="#">Registrar producto</a></li>
                  <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Productos</a>
                      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <li><a class="dropdown-item" href="#">Todos los productos</a></li>
                          <li><hr class="dropdown-divider" /></li>
                          <li><a class="dropdown-item" href="#">Más vendidos</a></li>
                          <li><a class="dropdown-item" href="#">Tecnología</a></li>
                          <li><a class="dropdown-item" href="#">Hogar</a></li>
                      </ul>
                  </li>
              </ul>
              <form class="d-flex">
                  <button class="btn btn-outline-dark" type="submit"><img class="icono" type="image/x-icon" src="imagenes/iconos/carrito_compras.ico">
                      Carrrito
                      <span class="badge bg-dark text-white ms-1 rounded-pill">0</span>
                  </button>
              </form>
            </div>
          </div>
        </nav>
        <!-- Header-->
      <header class="pt-5 linea">
        <hr>
        <div class="ps-5 titulos">
            <h2>Registro De Productos: </h2>
        </div>
        <hr>
      </header>
      <section class="pt-1 pb-5">
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
        <?php
            @$link = new mysqli('localhost', 'root', 'VIDAL2207l', 'tiendaonline');
            if ($link->connect_errno) 
            {
                echo '<script type="text/javascript">';
                echo'error_conexion();';
                echo'</script>';
            }
            $sql = "INSERT INTO producto VALUES (null, '{$nombre}', '{$marca}', '{$modelo}', {$precio}, '{$detalles}', {$unidades}, '{$imagen}')";
            if ( $link->query($sql) ) 
            {
                $id = $link->insert_id;
//cerrad?> 
                <h2 class="titulo">Producto registrado:</h2>
                <div class="formulario">
                    <p class="muestra">Producto insertado con ID: <?php echo $id; ?></p>
                    <p class="muestra">Nombre: <?php echo $nombre; ?></p>
                    <P class="muestra">Marca: <?php echo $marca; ?></p>
                    <p class="muestra">Modelo: <?php echo $modelo; ?></p>
                    <p class="muestra">Precio: <?php echo $precio; ?></p>
                    <p class="muestra">Dettalles: <?php echo $detalles; ?></p>
                    <p class="muestra">Unidades: <?php echo $unidades; ?></p>
                    <p class="muestra">Imagen: <?php echo $imagen; ?></p>
                </div>
                <form action="formulario.html">
                    <button type="submit" class="boton">Regresar</button>
                </form>
        <?php //ABIERTO 
            }
            else{
              echo '<script type="text/javascript">';
              echo'advertencia();';
              echo'</script>';
            }
            $link->close();
          ?>
        </div>
      </section>
    </body>
    <footer class="py-3 bg-dark">
      <div class="container"><p class="m-0 text-center text-white">&copy; Copyright <strong>Tienda Online</strong>. All Rights Reserved RVL</p></div>
      <!-- Bootstrap core JS-->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </footer>
</html>