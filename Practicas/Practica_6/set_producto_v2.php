<?php
    $nombre = $_POST['nombre'];
    $marca  = $_POST['marca'];
    $modelo = $_POST['modelo'];
    $precio = $_POST['precio'];
    $detalles = $_POST['detalles'];
    $unidades = $_POST['unidades'];
    $imgContenido = $_POST['file'];
    //$image = $_FILES['imagen'];
    //$imagen = addslashes(file_get_contents($image));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <link rel="stylesheet" type="text/css" href="css/estilos.css">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Practica 6</title>
  </head>
  <body>
      <header class="header">
        <nav class="nav">
          <a href="#" class="logo nav-link">MarketPlace</a>
          <ul class="nav-menu">
            <li class="nav-menu-item">
              <a href="#" class="nav-menu-link nav-link active">Registrar</a>
            </li>
            <li class="nav-menu-item">
              <a href="#" class="nav-menu-link nav-link">Mostrar</a>
            </li>
          </ul>
        </nav>
      </header>
      <section class="section">
        <?php //ABIERTO
            if(strlen($nombre)==0 or strlen($marca)==0 or strlen($modelo)==0 or strlen($precio)==0 or strlen($detalles)==0 or strlen($unidades)==0 or strlen($imgContenido)==0)
            {
                include("formulario_productos.html");
//cerrad?> 
                <div class="advertencia">
                    <p>Si no te resulta mucho esfuerzo vuelve a llenar los campos. <b class="red">CAMPOS VACIOS</b></p>
                </div>
        <?php //ABIERTO 
            }
            else
                {

                    @$link = new mysqli('localhost', 'root', 'VIDAL2207l', 'marketzone');	
                    /** comprobar la conexión */
                    if ($link->connect_errno) 
                    {
                        die('Falló la conexión: '.$link->connect_error.'<br/>');
                        /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */
                    }
                    /*$sl = "SELECT * FROM productos WHERE nombre='{$nombre}' AND marca=$'{marca}' AND modelo='{$modelo}'";
                    if( $link->query($sl) )
                    {
                        include("formulario_productos.html");
//cerrad?> 
                        <div class="advertencia">
                            <p>Ya existe un registro. <b class="red">PRODUCTO DUPLICADO</b></p>
                        </div>
        <?php //ABIERTO 
                    }
                    else
                    {*/
                    $sql = "INSERT INTO productos VALUES (null, '{$nombre}', '{$marca}', '{$modelo}', {$precio}, '{$detalles}', {$unidades}, '{$imgContenido}','0')";
                    if ( $link->query($sql) ) 
                        {
                            $id = $link->insert_id;
//cerrad?> 
                            <h2 class="titulo">Producto registrado:</h2>
                            <div class="formulario">
                                <p class="muestra">Producto insertado con ID: <?php echo $id; ?></p>
                                <p class="muestra">Nombre: <?php echo $nombre; ?></p>
                                <P class="muestra">Marca: <?php echo $marca; ?></p>
                                <p class="muestra">Modelo: <?php echo $modelo; ?></p>
                                <p class="muestra">Precio: <?php echo $precio; ?></p>
                                <p class="muestra">Dettalles: <?php echo $detalles; ?></p>
                                <p class="muestra">Unidades: <?php echo $unidades; ?></p>
                                <p class="muestra">Imagen: <?php echo $imgContenido; ?></p>
                            </div>
                            <form action="formulario_productos.html" method="post">
                                <button type="submit" class="boton">Regresar</button>
                            </form>
        <?php //ABIERTO 
                        }
                    else
                        {
                            include("formulario_productos.html");
//cerrad?> 
                            <div class="advertencia">
                                <p><b class="red">No se logro insertar el producto</b></p>
                            </div>
        <?php //ABIERTO 
                        }
                    //}
                    $link->close();
                }
//CERRADO?>
        </div>
      </section>
    </body>
  <footer class="footer">
    <div>
      <p>&copy; Copyright<strong> Ramiro Vidal Lumbreras</strong></p>
    </div>
  </footer>
</html>