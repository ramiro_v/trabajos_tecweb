<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<?php
        $data = array();
        /** SE CREA EL OBJETO DE CONEXION */
        @$link = new mysqli('localhost', 'root', 'VIDAL2207l', 'marketzone');	

        /** comprobar la conexión */
        if ($link->connect_errno) 
        {
            die('Falló la conexión: '.$link->connect_error.'<br/>');
                /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */
        }

        /** Crear una tabla que no devuelve un conjunto de resultados */
        if ( $result = $link->query("SELECT * FROM productos WHERE eliminado = '0'") ) 
        {
            /** Se extraen las tuplas obtenidas de la consulta */
            $row = $result->fetch_all(MYSQLI_ASSOC);

            /** Se crea un arreglo con la estructura deseada */
            foreach($row as $num => $registro) {            // Se recorren tuplas
                foreach($registro as $key => $value) {      // Se recorren campos
                    $data[$num][$key] = utf8_encode($value);
                }
            }

            /** útil para liberar memoria asociada a un resultado con demasiada información */
            $result->free();
        }
        $link->close();
	?>
	<head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <link rel="stylesheet" type="text/css" href="css/estilos.css">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Practica 6</title>
    </head>
    <body>
        <header class="header">
            <nav class="nav">
            <a href="formulario_productos.html" class="logo nav-link">MarketPlace</a>
                <ul class="nav-menu">
                    <li class="nav-menu-item">
                    <a href="formulario_productos.html" class="nav-menu-link nav-link">Registrar</a>
                    </li>
                    <li class="nav-menu-item">
                    <a href="get_productos_vigentes.php" class="nav-menu-link nav-link active">Mostrar</a>
                    </li>
                </ul>
            </nav>
        </header>
        <section class="section">
            <h2 class="titulo">Productos vigentes:</h2>
            <?php if( isset($data) )?>
                <table class="table" style="text-align:center;">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Modelo</th>
                        <th scope="col">Precio</th>
                        <th scope="col">Unidades</th>
                        <th scope="col">Detalles</th>
                        <th scope="col">Imagen</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($data as $arr){
                            echo'<tr>';
                                echo'<th scope="arr">' .$arr['id']. '</th>';
                                echo '<td>' .$arr['nombre']. '</td>';
                                echo '<td>'.$arr['marca']. '</td>';
                                echo '<td>' .$arr['modelo']. '</td>';
                                echo '<td>' .$arr['precio']. '</td>';
                                echo '<td>' .$arr['unidades']. '</td>';
                                echo '<td>'.$arr['detalles'].'</td>';
                                echo '<td><img style="width:140px;" src='.$arr['imagen'].'></td>';
                            echo'</tr>';
                        }
                    ?>
                    </tbody>
                </table>
            
        </section>
    </body>
</html>