<?php

    include_once __DIR__.'/database.php';
    // SE OBTIENE LA INFORMACIÓN DEL PRODUCTO ENVIADA POR EL CLIENTE
    $producto = file_get_contents('php://input');
    if(!empty($producto)) {
        // SE TRANSFORMA EL STRING DEL JASON A OBJETO
        $jsonOBJ = json_decode($producto);
        /**
         * SUSTITUYE LA SIGUIENTE LÍNEA POR EL CÓDIGO QUE REALICE
         * LA INSERCIÓN A LA BASE DE DATOS. COMO RESPUESTA REGRESA
         * UN MENSAJE DE ÉXITO O DE ERROR, SEGÚN SEA EL CASO.
         */
        $Nombre = $jsonOBJ->{'nombre'};
        
        //VALIDAR SI EL PRODUCTO EXISTA
        if ( $result = $conexion->query("SELECT * FROM productos WHERE nombre = '{$Nombre}' and eliminado = '0'") ) {
            // SE OBTIENEN LOS RESULTADOS
            $row = $result->fetch_array(MYSQLI_ASSOC); 
            if(!is_null($row)) {
                echo "El Nombre del producto ya existe";
            }
            else{
                $Marca = $jsonOBJ->{'marca'};
                $Modelo = $jsonOBJ->{'modelo'};
                $Unidades = $jsonOBJ->{'unidades'};
                $Detalles = $jsonOBJ->{'detalles'};
                $Imagen = $jsonOBJ->{'imagen'};
                $Precio = $jsonOBJ->{'precio'};

                $sql = "INSERT INTO productos VALUES (null, '{$Nombre}', '{$Marca}', '{$Modelo}', {$Precio}, '{$Detalles}', {$Unidades}, '{$Imagen}','0')";
                if ( $conexion->query($sql) ) 
                {
                    echo "Producto insertado con ID: ".$conexion->insert_id." :)";
                }
                else
                {
                    echo "El Producto no pudo ser insertado =(";
                }
            }
            $result->free();
        }
        else
        {
            echo "Fallo la conexión =(";
        }
        $conexion->close();
    //echo '[SERVIDOR] Nombre: '.$jsonOBJ->nombre;
    }   
    else{
        echo "El Producto no pudo ser insertado --> Campos vacios";
    }
?>