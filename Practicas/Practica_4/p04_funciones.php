<?php
    function ejercicio_1($numero1){
        if ($numero1%5 == 0){
            if ($numero1%7==0){
                echo ($numero1.' es multiplo de 5 y 7');
            }
            else{
                echo ($numero1.' solo es multiplo de 5 pero no de 7');
            }
        }
        else{
            echo ($numero1.' NO es multiplo de 5');
        }
    }
    
    function ejercicio_2(){
        $encontrado = false;
        $Matriz = array();
        $contador = 0;
        while ($encontrado == false){
            $_indice = 0;
            $arr = array(2);
            while ($_indice <= 2){
                $num = rand(111,999);
                $arr[$_indice]=$num;
                $_indice += 1;
            }
            if($arr[0]%2 != 0){
                if($arr[1]%2 == 0){
                    if($arr[2]%2 != 0){
                        $encontrado = true;
                    }
                }
            }
            array_push($Matriz,$arr);
            $contador += 1;
        };
        foreach($Matriz as $Fila){
            echo ($Fila[0].',&nbsp&nbsp'.$Fila[1].',&nbsp&nbsp'.$Fila[2]);
            echo '<br>';
        }
        echo '<br><b>'.(($contador*3).'</b>'.' números obtenidos en '.
        '<b>'.$contador.'</b>'.' iteraciones');
    }

    function ejercicio_3($numero){
        $num_encontrado = false;
        #while ($num_encontrado == false){
        #    $_aleatorio = rand(1,$numero);    
        #    if ($numero%$_aleatorio == 0){
        #        $num_encontrado = true;
        #        echo $_aleatorio.' es multiplo de '.$numero;
        #    }
        #}
        # -------------     VARIANTE CON DO-WHILE
        do{
            $_aleatorio = rand(1,$numero);    
            if ($numero%$_aleatorio == 0){
                $num_encontrado = true;
                echo $_aleatorio.' es multiplo de '.$numero;
            }
        }while ($num_encontrado == false);
    }

    function ejercicio_4(){
        $abecedario = array();
        for ($indice=97;$indice<=122;$indice++){
            $abecedario[$indice] = chr($indice);
        }
        foreach ($abecedario as $key => $value){
            echo '['.$key.'] => '.$value.'<br>';
        }
    }
?>