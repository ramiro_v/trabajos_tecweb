<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html"; charset="UTF-8" />
        <link rel="stylesheet" type="text/css" href="../css/estilos.css">
        <title>Formulario php</title>
    </head>
    <body>
        <header>
            <h1>Formulario</h1>
        </header>
        <section>
            <div style="text-align:center;">
                <?php
                    echo '<link rel="stylesheet" type="text/css" href="../css/estilos.css">';
                    $sexo = $_POST['sexo'];
                    $edad = $_POST['edad'];
                    echo '<div class="form tam">';
                        echo'<h2>Sus datos son:</h2>';
                        echo 'Sexo = '.$sexo.'<br>';
                        echo 'Edad = '.$edad.'<br><br>';
                        if ($sexo == 'femenino'){
                            if ($edad<35 and $edad>18){
                                echo 'Bienvenida, usted está en el rango de edad permitido.';
                            }
                            else{
                                echo '<b class="text-danger">ERROR</b> no cumple con la edad solicitada';
                            }
                        }
                        else{
                            echo '<b text-danger>ERROR</b> no cumple con el sexo solicitado';
                        }
                    echo '</div>';
                ?>
            </div>
        </section>
        <footer>
            <div  style="text-align:center;"><p>&copy; Copyright <strong>Ramiro Vidal Lumbreras</strong></p></div>
        </footer>
    </body>
</html>