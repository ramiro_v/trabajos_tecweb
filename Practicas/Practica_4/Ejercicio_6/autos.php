<?php
    function autos_por_matricula(){
        #MATRICULAS
        $matricula = array('RAM2207','RTY2109','MAR2107','PLO9898','LAM2022','XAM2207','RFG4545',
        'WDF2297','KJT2200','XZC3456','WWW1234','FGH6767','LOK0987','VRL7654','QWE4323');
        #DATOS DEL AUTO
        $marca = array('Honda','Mazda','Nissan','Kia','Kia','Mazda','Nissan','Ford','Kia','Kia',
        'Kia','Chevrolet','Honda','Mazda','Nissan');
        $modelo = array('2020','2019','2018','2022','2011','2003','2008','2022','2010','2009',
        '2008','2020','2019','2018','2022');
        $tipo = array('Sedan','Hachback','Camioneta','Sedan','Hachback','Sedan','Hachback','Camioneta','Sedan','Hachback',
        'Hachback','Camioneta','Sedan','Sedan','Camioneta');
        #DATOS DEL PROPIETARIO
        $nombre = array('Ramiro Vidal','Arturo Lopez','Carlos Perez','Alfonso Garcia','Jesus Manuel','Jose Eduardo','Carmen Santos',
        'Juan Antonio','Guillermo Perez','Arturo Salinas','Pepe Antonio','Roberto Fernandez','Hugo Quiroga','Ricardo Hernandez','Ana Montiel');
        $ciudad = array('Puebla, pue.','Tamaulipas,tam.','Nuevo Leon, mont.','Puebla, pue.','Tamaulipas,tam.','Nuevo Leon, mont.','Puebla, pue.',
        'Tamaulipas, tam.','Nuevo Leon, mont.','Puebla, pue.','Mexico, mex.','Baja California, tij.','Ecatepec, mex.','Puebla, pue.','Ecatepec, mex.');
        $direccion = array('Emiliano Zapata','Centro','Centro','Boulevard 5','Boulevard Serdan','14 poniente y 13 sur','Centro','Avenida Benito Juarez','Interseccion A km 107','Centro',
        'Boulevard','Avenida 45 y calle 11','Avenida 45 y calle 13','Centro','Privada B');
        
        $autos = array();
        #CREACION DEL ARREGLO QUE CONTENGA TODOS LOS DATOS SOLICITADOS
        #   MATRICULA =>
        #         Auto => MARCA - MODELO - TIPO
        #         Propietario => NOMBRE - CIUDAD - DIRECCION
        for($i=0;$i<15;$i++){
            $auto = ['Marca' => $marca[$i],'Modelo' => $modelo[$i],'Tipo' => $tipo[$i]];
            $Propietario=['Nombre'=>$nombre[$i],'Ciudad' => $ciudad[$i],'Dirección' => $direccion[$i]];
            $autos[$matricula[$i]] =['Auto' => $auto,'Propietario' => $Propietario];
        }
        return $autos;
    }
?>