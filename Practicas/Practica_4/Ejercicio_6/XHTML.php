<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html"; charset="UTF-8" />
        <link rel="stylesheet" type="text/css" href="../css/estilos.css">
        <title>Ejercicio 6</title>
    </head>
    <body>
        <header>
            <h1>Registro de autos</h1>
        </header>
        <section class="impresion-section">
            <div style="text-align:center;">
                <?php
                    echo '<link rel="stylesheet" type="text/css" href="../css/estilos.css">';
                    include('autos.php');
                    $boton = $_POST['boton'];
                    if ($boton == 1){
                        $_placa = $_POST['matricula_form'];
                        $_autos = autos_por_matricula();
                        $_encontrado = false;
                        echo '<div class="impresion">';
                            echo '<h3>Resultados de la búsqueda:</h3><br>';
                            foreach ($_autos as $matricula => $dato){
                                if ($_placa == $matricula){
                                    echo '<b>Matricula: ['.$matricula.']</b><br>';
                                    foreach($dato as $auto => $name){
                                        echo '<br><p class="bold-subtitulo">'.$auto.':</p>';
                                        foreach($name as $key => $valor){
                                            echo '<br>'.$key.' => '.$valor.' ';
                                        }
                                        echo '<br>';
                                    }
                                    echo '<br>';
                                    $_encontrado = true;
                                }
                            }
                            if ($_encontrado == false){
                                echo '<p class="error">Matricula no encontrada</p>';
                            }
                        echo '</div>';
                    }
                    else {
                        echo '<div class="impresion">';
                            echo '<h3>Estos son todos los registros:</h3><br>';
                            $_autos = autos_por_matricula();
                            foreach ($_autos as $matricula => $dato){
                                echo '<b>Matricula: ['.$matricula.']</b><br>';
                                foreach($dato as $auto => $name){
                                    echo '<br><p class="bold-subtitulo">'.$auto.':</p>';
                                    foreach($name as $key => $valor){
                                        echo '<br>'.$key.' => '.$valor.' ';
                                    }
                                    echo '<br>';
                                }
                                echo '<br>';
                            }
                        echo '</div>';
                    }
                ?>
                <form action="Formulario2.html" method="post">
                    <button type="submit" name="boton" class="btn">Regresar</button>
                 </form>
            </div>
        </section>
    </body>
</html>