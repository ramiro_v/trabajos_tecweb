<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Practica 3</title>
    </head>
    <body>
        <div style="text-align:center;">
            <h3 style="font-family:verdana;">Tecnologías Web</h3>
        </div>
        <?php
            echo ("<br>*********** PROBLEMA 1 ***********<br><br>");
            $_myvar = '$_myvar';
            $_7var = '$_7var';
            // myvar = 'myvar';        --------------------- FALTA "$" PARA QUE SEA CONSIDERADO COMO VARIABLE
            $myvar = '$myvar';
            $var7 = '$var7';
            $_element1 = '$_element1';
            // $house*5 = "$house*5";  --------------------- UN OPERADOR NO PUEDE SER TOMADO COMO VARIABLE
            echo ($_myvar."  ------ SI ES VARIABLE<br>");
            echo ($_7var."   ------ SI ES VARIABLE<br>");
            echo ($myvar."   ------ SI ES VARIABLE<br>");
            echo ($var7."    ------ SI ES VARIABLE<br>");
            echo ($_element1."   -- SI ES VARIABLE<br>");
            unset($myvar,$_7var,$myvar,$var7,$_element1);

            echo ("<br>*********** PROBLEMA 2 ***********<br><br>");
            $a = "ManejadorSQL";
            $b = 'MYSQL';
            $c = &$a;
            echo ('$a = '.$a."<br>");
            echo ('$b = '.$b."<br>");
            echo ('$c = '.$c."<br>");
            $a = "PHP server"; // LA VARIABLE a ES NUEVAMENTE ASIGNADA
            $b = &$a;           // b ES REFERENCIADA A LA VARIABLE $a (SI SE MODIFICARA a TAMBIEN LA VARIABLE b)
            echo ('$a = '.$a."<br>"); 
            echo ('$b = '.$b."<br>");
            unset($a,$b,$c);

            echo ("<br>*********** PROBLEMA 3 ***********<br><br>");
            $a = "PHP5";
            echo ('$a = '.$a."<br>");
            $z[] = &$a;   // -------- SE REFERENCIA LA VARIABLE $a A z EN LA POSICION 0 (ES EL PRIMER VALOR DEL ARREGLO)
            echo('$z[] = ');
            print_r($z);
            $b = " 5a version de PHP";
            echo ('$b = '.$b."<br>");
            //$c = $b*10;  // ---------------------- NO SE PUEDE HACER OPERACIONES MATEMATICAS CON UN STRING
            //echo ('$c = '.$c."<br>");
            $a .= $b;  // -------------------------- .= CONCATENA 2 STRINGS EN UNA VARIABLE
            echo ('$a = '.$a."<br>");
            //$b *= $c;
            //echo ('$b = '.$b."<br>");
            $z[0] = "MySQL";  // ------------------- SE VUELVE A ASIGNAR Z[0] 
            echo('$z[] = ');
            print_r($z);
            //FUNCION ***** problema 4
            function problema_4(){
                echo ("<br>*********** PROBLEMA 4 ***********<br><br>");
                echo ('$a == '.$GLOBALS["a"]."<br>");
                echo ('$b == '.$GLOBALS["b"]."<br>");
                echo ('$z == ');
                print_r($GLOBALS["z"]);
            }
            //
            problema_4();
            unset($a,$b,$z);

            echo ("<br>*********** PROBLEMA 5 ***********<br><br>");
            $a = "7 personas";
            echo ('$a = '.$a."<br>"); 
            $b = (integer) $a; //TRANSFORMA UNA VARIABLE EN ENTERO
            $a = "9E3";
            $c = (double) $a; //TRANSFORMAR VARIABLE EN DOUBLE 9E3 == 9000
            echo ('$b = '.$b."<br>");
            echo ('$a = '.$a."<br>");
            echo ('$c = '.$c."<br>");
            //PARA EL PROBLEMA 6 DEJO LAS VARIABLES EXISTENTES DEL PROBLEMA 5
            echo ("<br>*********** PROBLEMA 6 ***********<br><br>");
            $d = false;
            $e= true;
            $f = false;
            var_dump($a,$b,$c,$d,$e,$f);
            echo ("<br>*********** PROBLEMA 7 ***********<br><br>");
            echo('La version de Apache y PHP: '. $_SERVER['SERVER_SOFTWARE']);
            echo('Sistema operativo: '. $_SERVER['HTTP_USER_AGENT']);
            echo("Idioma del navegador: ". $_SERVER['HTTP_ACCEPT_LANGUAGE']);
        ?>
        <p>
        <a href="http://validator.w3.org/check?uri=referer"><img
            src="http://www.w3.org/Icons/valid-xhtml11" alt="Valid XHTML 1.1" height="31" width="88" /></a>
        </p>
    </body>
</html>