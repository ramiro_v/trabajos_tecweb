<?php
    use Tiendaram\Read\Read as R;
    require_once __DIR__ . '/../vendor/autoload.php';
    //include_once __DIR__.'/Read/product-read.php';
    $name = $_POST['name']; 
    if( isset($name) ) {
        $productos = new R('marketzone');
        $productos->byname($name);
        echo $productos->getResponse();
    } 
?>