<?php
    namespace Tiendaram\Read;
    use Tiendaram\API\DataBase as BD;
    require_once __DIR__ . '/../../vendor/autoload.php';
    //include_once __DIR__.'/../API/database.php';
    class Read extends BD{

        public function list(){
            if ( $result = $this->conexion->query("SELECT * FROM productos WHERE eliminado = 0") ) {
                $rows = $result->fetch_all(MYSQLI_ASSOC);
                if(!is_null($rows)) {
                    foreach($rows as $num => $row) {
                        foreach($row as $key => $value) {
                            $this->response[$num][$key] = utf8_encode($value);
                        }
                    }
                } else{
                    $this->response = array(
                        'status:'  => 'ERROR',
                        'message:' => 'No hay productos'
                    );
                }
                $result->free();
            } else {
                die('Query Error: '.mysqli_error($this->conexion));
            }
        }

        public function search($search){
            $sql = "SELECT * FROM productos WHERE (id = '{$search}' OR nombre LIKE '{$search}%' OR marca LIKE '{$search}%' OR detalles LIKE '{$search}%') AND eliminado = 0";
            if ( $result = $this->conexion->query($sql) ) {
                $rows = $result->fetch_all(MYSQLI_ASSOC);
                if(!is_null($rows)) {
                    foreach($rows as $num => $row) {
                        foreach($row as $key => $value) {
                            $this->response[$num][$key] = utf8_encode($value);
                        }
                    }
                }
                $result->free();
            } else {
                die('Query Error: '.mysqli_error($this->conexion));
            }
        }

        public function byname($name){
            $this->response['status'] =  "error";
            $sql = "SELECT * FROM productos WHERE nombre = '{$name}'";
            if ( $result = $this->conexion->query($sql) ) {
                $row = $result->fetch_array(MYSQLI_ASSOC);
                if(!is_null($row)) {
                    $this->response['status'] =  "success";
                }
                $result->free();
            } else {
                die('Query Error: '.mysqli_error($this->conexion));
            }
        } 
        
        public function byid($id){
            if ( $result = $this->conexion->query("SELECT * FROM productos WHERE id ={$id}") ) {
                $rows = $result->fetch_all(MYSQLI_ASSOC);
                if(!is_null($rows)) {
                    foreach($rows as $num => $row) {
                        foreach($row as $key => $value) {
                            $this->response[$num][$key] = utf8_encode($value);
                        }
                    }
                }
                else{
                    $this->response = array(
                        'Estatus:'  => 'ERROR',
                        'Mensaje:' => 'No hay productos'
                    );
                }
                $result->free();
            } else {
                die('Query Error: '.mysqli_error($this->conexion));
            }
        }
    }
?>