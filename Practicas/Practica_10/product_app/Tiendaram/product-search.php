<?php
    use Tiendaram\Read\Read as R;
    require_once __DIR__ . '/../vendor/autoload.php';
    //include_once __DIR__.'/Read/product-read.php';
    $search = $_POST['search']; 
    if( isset($search) ) {
        $productos = new R('marketzone');
        $productos->search($search);
        echo $productos->getResponse();
    } 
?>