<?php
    use Tiendaram\Read\Read as R;
    require_once __DIR__ . '/../vendor/autoload.php';
    //include_once __DIR__.'/Read/product-read.php';
    $id = $_POST['id']; 
    if( isset($id) ) {
        $productos = new R('marketzone');
        $productos->byid($id);
        echo $productos->getResponse();
    } 
?>