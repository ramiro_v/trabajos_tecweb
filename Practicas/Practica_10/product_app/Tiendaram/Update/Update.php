<?php
    namespace Tiendaram\Update;
    use Tiendaram\API\DataBase as BD;
    require_once __DIR__ . '/../../vendor/autoload.php';
    //include_once __DIR__.'/../API/database.php';
    class Update extends BD{
        public function edit($producto){
            $this->response = array(
                'status'  => 'error',
                'message' => 'Error en el producto'
            );
            $jsonOBJ = json_decode($producto);
            $id = $jsonOBJ->id;
             if(!empty($id)) {
                $nombre = $jsonOBJ->nombre;
                $marca  = $jsonOBJ->marca;
                $modelo = $jsonOBJ->modelo;
                $precio = $jsonOBJ->precio;
                $detalles = $jsonOBJ->detalles;
                $unidades = $jsonOBJ->unidades;
                $imagen = $jsonOBJ->imagen;
                $sql = "UPDATE productos SET nombre='$nombre', marca='$marca', modelo='$modelo', precio='$precio', detalles='$detalles', unidades='$unidades', imagen='$imagen', eliminado='0' WHERE id='$id'";	
                if(!($this->conexion->query($sql))){
                    die('Query Failed.');
                    $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
                }
                else{
                    $this->response['status'] =  "success";
                    $this->response['message'] =  "Producto ".$nombre." actualizado";
                }   
             }
        }
    }

?>