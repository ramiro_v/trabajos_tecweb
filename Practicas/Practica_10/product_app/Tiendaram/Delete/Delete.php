<?php
    namespace Tiendaram\Delete;
    use Tiendaram\API\DataBase as BD;
    require_once __DIR__ . '/../../vendor/autoload.php';
    //include_once __DIR__.'/../API/database.php';
    class Delete extends BD{
        public function eliminar($id){
            $this->response = array(
                'status'  => 'error',
                'message' => 'La consulta falló'
            );
            if($id!=''){
                $sql = "UPDATE productos SET eliminado=1 WHERE id = {$id}";
                if ( $this->conexion->query($sql) ) {
                    $this->response['status'] =  "success";
                    $this->response['message'] =  "Producto eliminado";
                } else {
                    $this->response['status'] =  "error";
                    $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
                }
            } 
        }
    }

?>